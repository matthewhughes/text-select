SRCDIR := src
INCDIR := include
OBJDIR := obj

TARGET := text_select
SRCS := $(wildcard $(SRCDIR)/*.c)
OBJS := $(SRCS:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

CFLAGS := -Wall -Wextra -Wpedantic -Werror $(EXTRA_CFLAGS)
CPPFLAGS := -I$(INCDIR) -MMD -MP
LDFLAGS = -lncurses

PREFIX ?= /usr/local
INSTALL_DIR := $(DESTDIR)$(PREFIX)/bin
INSTALL_TRGT := $(INSTALL_DIR)/$(TARGET)

.DEFAULT: $(TARGET)
$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

$(OBJDIR):
	mkdir --parents $@

.PHONY: clean
clean:
	@$(RM) --verbose --recursive $(TARGET) $(OBJDIR)

.PHONY: install
install: $(TARGET)
	install --verbose --directory '$(INSTALL_DIR)' && \
		install --verbose '$(TARGET)' '$(INSTALL_TRGT)'

.PHONY: uninstall
uninstall: $(INSTALL_TRGT)
	rm --force '$(INSTALL_TRGT)'

-include $(OBJ:.o=.d)
