# Text Select

A curses interface to select words from stdin:

    $ printf "foo\nbar\nbuz\n" | text_select

Keys:

* `q` quit
* `Enter` confirm selection
* arrow keys or `h`, `j`, `k`, `l` to navigate
