#pragma once
/*
 * General utils, not tied to ncurses
 */
#define BUFSIZE 128

struct Line
{
    size_t len;
    char *text;
};

void die(const char *fmt, ...);
FILE *get_tty(FILE *target, const char *mode);

int read_lines(struct Line *lines);
void free_lines(struct Line *lines, int line_count);
