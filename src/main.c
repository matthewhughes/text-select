#include <curses.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"

/*  init_colours - initialise colours for curses session */
void
init_colours()
{
    start_color();
    use_default_colors();
    init_pair(1, COLOR_WHITE, -1);
    init_pair(2, COLOR_BLACK, COLOR_WHITE);
}

SCREEN *
init_screen(FILE *out, FILE *in, int *rows, int *cols)
{
    SCREEN *screen = newterm(NULL, out, in);
    set_term(screen);
    init_colours();
    cbreak();
    noecho();
    keypad(curscr, TRUE);
    getmaxyx(curscr, *rows, *cols);
    return screen;
}

void
draw_helper_window(WINDOW *helper_win)
{
    const char *help_text = "q: quit, <enter>: confirm selection";
    box(helper_win, 0, 0);
    mvwprintw(helper_win, 1, 1, "%s", help_text);
    wrefresh(helper_win);
}

/* print_lines - print the command output; the output of `get_lines` to a window */
void
print_lines(WINDOW *win, const struct Line *lines, int line_count)
{
    int start_x, start_y;
    getyx(win, start_x, start_y);
    for (int i = 0; i < line_count; i++)
        waddnstr(win, lines[i].text, getmaxx(win));
    wmove(win, start_y, start_x);
}

/* highlight_word - Highlight the word starting at current cursor position
 *
 * The cursor will be moved back to it's original position.
 */
void
highlight_word(WINDOW *win, int pair_id)
{
    int start_x, start_y;
    getyx(win, start_y, start_x);

    int x = start_x;
    chtype c;
    while ((c = mvwinch(win, start_y, x) & A_CHARTEXT) != ' '
           && x < getmaxx(win)) {
        waddch(win, c | COLOR_PAIR(pair_id));
        x++;
    }
    wmove(win, start_y, start_x);
}

/* find_next_word - Find the next word, starting at current cursor position
 *
 * @return The cursor column position of the next word
 *
 * If the current word is on the far right of the window, wraps around to the
 * opposite side.
 * The cursor will be moved back to it's original position.
 */
int
find_next_word(WINDOW *win)
{
    int start_x;
    int start_y;
    getyx(win, start_y, start_x);
    const int cols = getmaxx(win);

    int x = start_x;

    // Goto end of current word
    while ((mvwinch(win, start_y, x + 1) & A_CHARTEXT) != ' ' && x < cols)
        x++;

    // find next word
    while ((mvwinch(win, start_y, x + 1) & A_CHARTEXT) == ' ' && x < cols)
        x++;

    // If the last space is at the end, just wrap back to start
    if (x == cols) {
        wmove(win, start_y, 0);
        if ((winch(win) & A_CHARTEXT) != ' ')
            x = 0;
        else
            x = find_next_word(win);
    } else {
        // Otherwise next word must be at next position
        x++;
    }

    wmove(win, start_y, start_x);
    return x;
}

/* find_prev_word - Find the previous word, starting at current cursor position
 *
 * @return The cursor column position of the previous word
 *
 * If the current word is on the edge of the window, wraps around to the
 * opposite side.
 * The cursor will be moved back to it's original position.
 */
int
find_prev_word(WINDOW *win)
{
    int start_x;
    int start_y;
    getyx(win, start_y, start_x);
    const int cols = getmaxx(win);

    int x = start_x;

    // find previous word
    while ((mvwinch(win, start_y, x - 1) & A_CHARTEXT) == ' ' && x > 0)
        x--;

    if (x == 0) {
        wmove(win, start_y, cols - 1);
        if ((winch(win) & A_CHARTEXT) != ' ')
            x = cols - 1;
        else
            x = find_prev_word(win);
    }
    // move to the start of the word
    while ((mvwinch(win, start_y, x - 1) & A_CHARTEXT) != ' ' && x > 0)
        x--;

    wmove(win, start_y, start_x);
    return x;
}

/* get_word - Get the word starting at the current position of the cursor
 *
 * @return - The word as a string, this must be freed 
 */
char *
get_word(WINDOW *win)
{
    int start_x, start_y;
    getyx(win, start_y, start_x);

    int x = start_x;
    int y = start_y;
    chtype c;
    while ((c = mvwinch(win, y, x) & A_CHARTEXT) != ' ')
        x++;
    if (x == start_x)
        return NULL;

    char *result = (char *) malloc(x - start_x);
    for (int i = start_x; i <= x; i++)
        result[i - start_x] = mvwinch(win, y, i) & A_CHARTEXT;
    return result;
}

/* get_selection - main loop to get the selected text
 *
 * @return - The selected string, this must be freed 
 * */
char *
get_selection(WINDOW *win, int line_count)
{
    unsigned x = 0;
    unsigned y = 0;
    wmove(win, y, x);

    highlight_word(win, 2);
    wrefresh(win);
    while (true) {
        bool made_move = false;
        switch (wgetch(win)) {
        case 'q':
            return NULL;
        case 'j':
            /* fall through */
            ;
        case KEY_DOWN:
            y = (y + 1) % line_count;
            made_move = true;
            break;
        case 'k':
            /* fall through */
            ;
        case KEY_UP:
            y = (y - 1) % line_count;
            made_move = true;
            break;
        case 'l':
            /* fall through */
            ;
        case KEY_RIGHT:
            x = find_next_word(win);
            made_move = true;
            break;
        case 'h':
            /* fall through */
            ;
        case KEY_LEFT:
            x = find_prev_word(win);
            made_move = true;
            break;
        case '\n':
            highlight_word(win, 1);
            return get_word(win);
        }
        if (made_move) {
            // clear highlighting on current word
            highlight_word(win, 1);
            wmove(win, y, x);
            // highlight new word
            highlight_word(win, 2);
            wrefresh(win);
        }
    }

}

int
main(void)
{
    int rows;
    int cols;

    FILE *out = get_tty(stdout, "w");
    if (out == NULL)
        die("Failed to open output: %s\n", strerror(errno));

    FILE *in = get_tty(stdin, "r");
    if (out == NULL)
        die("Failed to open input: %s\n", strerror(errno));

    struct Line *lines = (struct Line *) calloc(BUFSIZE, sizeof(struct Line));
    int line_count = read_lines(lines);

    SCREEN *screen = init_screen(out, in, &rows, &cols);
    WINDOW *main_win = newwin_sp(screen, rows - 3, cols, 0, 0);
    WINDOW *helper_win = newwin_sp(screen, 3, cols, rows - 3, 0);
    draw_helper_window(helper_win);

    print_lines(main_win, lines, line_count);

    char *selection = get_selection(main_win, line_count);
    endwin();
    delscreen(screen);

    free_lines(lines, line_count);
    if (selection != NULL) {
        printf("%s\n", selection);
        free(selection);
    }
    return 0;
}
