#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "utils.h"

/* die - Print a formatted message to stderr and exit
 *
 * Note: this does not handle any cleanup of curses
 */
void
die(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
    exit(EXIT_FAILURE);
}

/* get_tty - Open a TTY with mode, if target is a TTY use that
 */
FILE *
get_tty(FILE *target, const char *mode)
{
    if (isatty(fileno(target)))
        return target;
    else {
        errno = 0;
        return fopen("/dev/tty", mode);
    }
}

/*  get_lines - read lines from stdin to be displayed in curses
 *  @param lines - lines read
 *
 *  `lines` must have been already allocated, and may be reallocated
 */
int
read_lines(struct Line *lines)
{
    int i = 0;
    size_t len;
    ssize_t nread;
    errno = 0;
    while ((nread = getline(&lines[i].text, &len, stdin)) != -1) {
        lines[i].len = strlen(lines[i].text);
        if (i > 0 && i % (BUFSIZE - 1) == 0)
            lines = (struct Line *) realloc(lines, i * sizeof(struct Line));
        i++;
    }
    if (errno != 0)
        die("Failed reading input: %s\n", strerror(errno));
    return i;
}

void
free_lines(struct Line *lines, int line_count)
{
    for (int i = 0; i <= line_count; i++)
        free(lines[i].text);
    free(lines);
}
